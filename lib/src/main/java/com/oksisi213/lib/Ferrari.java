package com.oksisi213.lib;

/**
 * Created by Charles on 2018. 1. 28..
 */

public class Ferrari extends Car {
    //상속 다형성

    public Ferrari(String id) {
        super(id);
    }

    @Override
    public void accelate() {
        super.accelate();
        System.out.println("accelate called from 'Ferrari'");
        velocity += 20;
    }

    public void accelate(int velocity) {
        this.velocity += velocity;
    }

    public void openRoof() {
        System.out.print("open the Roof");
    }
}
