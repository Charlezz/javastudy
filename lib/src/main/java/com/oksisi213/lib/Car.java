package com.oksisi213.lib;

/**
 * Created by Charles on 2018. 1. 28..
 */

public class Car {

    private String id;

    protected int velocity = 0;

    public void accelate() {
        System.out.println("accelate called from 'Car'");
        velocity += 10;
    }

    public void deaccelate() {
        velocity -= 10;
    }

    public void checkVelocity() {
        System.out.println(id + ":velocity is " + velocity + "km/h");
    }


    private Car() { //기본 생성자

    }

    public Car(String id) {
        this.id = id;
    }
}

//메소드의 구조
/**
 * 접근제어자 반환형 메소드이름(인자값){
 * <p>
 * return 반환값
 * <p>
 * }
 * <p>
 * 접근제어자 클래스이름(인자){
 * <p>
 * }
 */


//생성자의 구조

/**
 * 접근제어자 클래스이름(인자){
 *
 * }
 *
 */