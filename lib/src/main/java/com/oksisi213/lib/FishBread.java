package com.oksisi213.lib;

/**
 * Created by Charles on 2018. 1. 21..
 */

public class FishBread {

    public String name;

    public FishBread() {//기본 생성자, 생략 가능, 생성자는 클래스명과 동일, 리턴타입 X
        name = "default";
    }

    public FishBread(String fishName) {
        name = fishName;
    }

    public FishBread(int size) {
    }

    public void sayMyName( ) { //리턴타입 , 함수명(동사+명사)
        System.out.println("I am " + name);
    }

}
