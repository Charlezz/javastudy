package com.oksisi213.lib;

/**
 * Created by Charles on 2018. 3. 10..
 */

public class Exam03 {

    static int maxDifference(int[] a) {
        int maxDiff = -1;

        if (a.length < 2) {
            System.out.println("입력값은 2개 이상이어야 합니다.");
            return -1;
        }

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < i; j++) {
                int diff = a[i] - a[j];
                if (diff > maxDiff) {
                    maxDiff = diff;
                }
            }
        }

        if (maxDiff == -1) {
            System.out.println("입력값 중 어느 것도 해당하는 조건을 만족하지 않습니다.");
        }

        return maxDiff;
    }

    public static void main(String[] args) {
        System.out.println(maxDifference(new int[]{7, 2, 3, 10, 2, 4, 8, 1}));

    }
}