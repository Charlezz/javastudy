package com.oksisi213.lib;

public class myClass {
    public static void main(String[] args) {
//        //변수(Variable), 상수(Constant)
//
//        //변수(값을 연산하는 경우, 바꾸는 경우 등)
//        int num = 10;   // 자료형 변수명 = 값(Value)
//        String name = "Charles";
//        char a = 'a';
//
//        //상수(서버주소, key 값..)
//
//        //일반적인 상수
//        //ex) "hello world", 100, etc...
//
//        //리터럴 상수
//        final int number = 10; //리터럴 상수
//        //number = 100;(x)
//
//
//        //자료형
//        //정수형 자료형
//        //int, long, float, double... 원시자료형 primitive type // 박싱, 언박싱
//        int age = 19; //4bytes
//        long time = 1l;//8bytes
//        float alpha = 1.0f;//4bytes
//        double pi = 1.0;//8bytes
//
//        //문자형 자료형
//        char b = 'b';
//        char[] banana = new char[]{'b', 'a', 'n', 'a', 'n', 'a'};
//        String banana2 = "banana";
//
//
//        //클래스, 오브젝트(인스턴스,객체),생성자,메서드(함수)
//
//
//        //클래스: 붕어빵틀
//        //오브젝트:붕어빵
//        //붕어빵을 만드려면?(인스턴스를 만드려면?)
//        //자바의 키워드 -> new
//
//
//        FishBread defaultFB = new FishBread();
//        FishBread nimoFB = new FishBread("Nimo");
//        FishBread sizeFB = new FishBread(100);
//
//
//        //메서드: 행위, 액션, 기능
//        defaultFB.sayMyName();
//        nimoFB.sayMyName();
//        sizeFB.sayMyName();
//
//        Calculator cal = new Calculator();
//        cal.plus(1, 1);
//
//        Calculator cal2 = new Calculator();
//        cal2.minus(1, 1);
//
//

        ///////////////////////////////////////////////////

//        Ferrari ferrari = new Ferrari("F-01");
//
//        Car car = new Car("C-01");//기본생성자
//
//        car.checkVelocity();
    }
}
