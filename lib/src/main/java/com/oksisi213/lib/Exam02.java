package com.oksisi213.lib;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Charles on 2018. 3. 10..
 */

public class Exam02 {

    static class SinglyLinkedListNode {
        public int data;
        public SinglyLinkedListNode next;

        public SinglyLinkedListNode(int nodeData) {
            this.data = nodeData;
            this.next = null;
        }
    }

    static class SinglyLinkedList {
        public SinglyLinkedListNode head;
        public SinglyLinkedListNode tail;

        public SinglyLinkedList() {
            this.head = null;
            this.tail = null;
        }

        public void insertNode(int nodeData) {
            SinglyLinkedListNode node = new SinglyLinkedListNode(nodeData);

            if (this.head == null) {
                this.head = node;
            } else {
                this.tail.next = node;
            }
            this.tail = node;
        }
    }

    public static void printSinglyLinkedList(SinglyLinkedListNode node, String sep, BufferedWriter bw) throws IOException {
        while (node != null) {
            bw.write(String.valueOf(node.data));

            node = node.next;

            if (node != null) {
                bw.write(sep);
            }
        }
    }

    static SinglyLinkedListNode deleteOdd(SinglyLinkedListNode listHead) {
        SinglyLinkedListNode head = listHead;

        while (head.data % 2 != 0) {
            head = head.next;
        }


        SinglyLinkedListNode node = head;
        while (true) {
            if (node.next != null && node.next.data % 2 != 0) {
                node.next = node.next.next;
            } else if (node.next == null) {
                break;
            }

        }

        return head;
    }

    private static final Scanner scan = new Scanner(System.in);


    public static void main(String[] args) throws IOException {
//        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        SinglyLinkedList listHead = new SinglyLinkedList();

//        int listHeadCount = Integer.parseInt(scan.nextLine().trim());
        int listHeadCount = 5;

        listHead.insertNode(2);
        listHead.insertNode(1);
        listHead.insertNode(3);
        listHead.insertNode(4);
        listHead.insertNode(6);

        SinglyLinkedListNode res = deleteOdd(listHead.head);


//
//        printSinglyLinkedList(res, "\n", bw);
//        bw.newLine();
//
//        bw.close();
    }
}
