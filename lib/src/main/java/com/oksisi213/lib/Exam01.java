package com.oksisi213.lib;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

public class Exam01 {

    static String[] reformatingDate(String[] dates) {
        String[] resultString = new String[dates.length];

        for (int i = 0; i < dates.length; i++) {
            String[] splitString = dates[i].split(" ");

            System.out.println("length =  " + splitString.length);
            if (splitString.length < 3) {
                System.out.println("입력오류 " + i);
                continue;
            }

            int month = (int) monthMap.get(splitString[1]);

            int year = Integer.parseUnsignedInt(splitString[2]);

            boolean c0 = Pattern.matches("^[0-9]*$", String.valueOf(splitString[0].charAt(0)));
            boolean c1 = Pattern.matches("^[0-9]*$", String.valueOf(splitString[0].charAt(1)));
            boolean c2 = Pattern.matches("^[0-9]*$", String.valueOf(splitString[0].charAt(1)));

            int date = -1;

            if (c0) {
                System.out.println("date 입력 오류" + i);
                continue;
            }

            if (!c1) {
                date = Integer.parseUnsignedInt(splitString[0].substring(0, 1));
            } else if (!c2) {
                date = Integer.parseUnsignedInt(splitString[0].substring(0, 2));
            }

            //final result check
            if (month > 12) {
//                System.out.println("month 는 12 를 넘을 수 없습니다." + i);
                continue;
            }

            if (date > 31) {
//                System.out.println("date 는 31 을 넘을 수 없습니다." + i);
                continue;
            } else if (date > 30) {
                if (month < 7) {
                    if (month % 2 == 0) {
//                        System.out.println(month + "월은 31일이 없습니다." + i);
                    }
                } else if (month == 9 || month == 11) {
//                    System.out.println(month + "월은 31일이 없습니다." + i);
                }
            } else if (date > 29) {
                if (month == 2) {
//                    System.out.println(month + "월은 30일이 없습니다." + i);
                }
            }

            if (year < 1900 || year > 2100) {
//                System.out.println("year 는 1900~2100 사이여야 합니다." + i);
                continue;
            }

            resultString[i] = year + "-" + String.format("%02d", month) + "-" + String.format("%02d", date);
        }

        return resultString;
    }

    static HashMap monthMap = new HashMap();

    public static void main(String[] args) throws ParseException {

        String str = "25th Oct 2052";


        str.split(" ");





        for(String s  : reformatDate(new String[]{str})){
            System.out.println(s);
        }

    }

    static String[] reformatDate(String[] dates) throws ParseException {
        String[] newDates = new String[dates.length];

        for (int i = 0; i < dates.length; i++) {

            String[] splitStr = dates[i].split(" ");
            int day = 0;
            if (splitStr[0].length() == 4) {
                day = Integer.parseInt(splitStr[0].substring(0, 2));
            } else if (splitStr[0].length() == 3) {
                day = Integer.parseInt(splitStr[0].substring(0, 1));
            }

            String strDay = String.format("%02d", day);


            SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-");

            Date date = sdf.parse(splitStr[1] + " " + splitStr[2]);


            newDates[i] = sdf1.format(date) + strDay;

        }
        return newDates;
    }
}