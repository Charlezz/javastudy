package com.oksisi213.javastudy;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = MainActivity.class.getSimpleName();

    Button button1;
    Button button2;
    Button button3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        LinearLayout li = new LinearLayout(this);

        TextView tv = new TextView(this);
        ImageView iv = new ImageView(this);

        li.addView(tv);
        li.addView(iv);

        TextView hello;
        hello = findViewById(R.id.hello_view);


        button1 = findViewById(R.id.btn1);
        button2 = findViewById(R.id.btn2);
        button3 = findViewById(R.id.btn3);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "BUTTON1", Toast.LENGTH_SHORT).show();
            }
        });


        View.OnClickListener listener1 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "BUTTON2", Toast.LENGTH_SHORT).show();
            }
        };
        View.OnClickListener listener2 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "BUTTON2", Toast.LENGTH_SHORT).show();
            }
        };
        View.OnClickListener listener3 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "BUTTON2", Toast.LENGTH_SHORT).show();
            }
        };

        button1.setOnClickListener(listener);
        button2.setOnClickListener(listener);
        button3.setOnClickListener(listener);


        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);


        /////////////////////////////////
        final Switch mySwitch = findViewById(R.id.my_switch);

        final CheckBox checkBox = findViewById(R.id.checkbox);

        final RadioGroup radioGroup = findViewById(R.id.radio_group);


        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(MainActivity.this, "switch:" + isChecked, Toast.LENGTH_SHORT).show();
            }
        });


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(MainActivity.this, "checkbox:" + isChecked, Toast.LENGTH_SHORT).show();
            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_button1:
                        Toast.makeText(MainActivity.this, "radio1", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radio_button2:
                        Toast.makeText(MainActivity.this, "radio2", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radio_button3:
                        Toast.makeText(MainActivity.this, "radio3", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });


        findViewById(R.id.check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = "";
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.radio_button1:
                        name = "radio1";
                        break;
                    case R.id.radio_button2:
                        name = "radio2";
                        break;
                    case R.id.radio_button3:
                        name = "radio3";
                        break;
                }
                Toast.makeText(MainActivity.this, "switch:" + mySwitch.isChecked() +
                        "\ncheckbox:" + checkBox.isChecked() +
                        "\nradio:" + name, Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) { //id = int
//            case R.id.btn1:
//                Toast.makeText(MainActivity.this, "BUTTON1", Toast.LENGTH_SHORT).show();
//                break;
//            case R.id.btn2:
//                Toast.makeText(MainActivity.this, "BUTTON2", Toast.LENGTH_SHORT).show();
//                break;
//            case R.id.btn3:
//                Toast.makeText(MainActivity.this, "BUTTON3", Toast.LENGTH_SHORT).show();
//                break;
//        }

        // == 연산자 : 값(Value)비교(int, float, boolean ...)
        // equals()  : 객체(Object) 비교 (String, object...)
        if (button1.equals(v)) {
            Toast.makeText(MainActivity.this, "BUTTON1", Toast.LENGTH_SHORT).show();
        } else if (v.equals(button2)) {
            Toast.makeText(MainActivity.this, "BUTTON2", Toast.LENGTH_SHORT).show();
        } else if (v.equals(button3)) {
            Toast.makeText(MainActivity.this, "BUTTON3", Toast.LENGTH_SHORT).show();
        }
    }

    public void myOnClick(View v) {
        Toast.makeText(MainActivity.this, "BUTTON4", Toast.LENGTH_SHORT).show();
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };
}
