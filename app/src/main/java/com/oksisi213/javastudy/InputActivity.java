package com.oksisi213.javastudy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InputActivity extends AppCompatActivity {

    private static final String TAG = InputActivity.class.getSimpleName();


    private static final int REQUEST_RESULT_ACTIVITY = 0;

    public static final String KEY_ID = "id";
    public static final String KEY_PW = "pw";

    public static final String KEY_RESULT_TEST = "Test";

    EditText email, password;
    Button next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        next = findViewById(R.id.next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = email.getText().toString();
                String pw = password.getText().toString();
                Intent intent = new Intent();
                intent.setClass(InputActivity.this, ResultActivity.class);
                intent.putExtra(KEY_ID, id);
                intent.putExtra(KEY_PW, pw);

                //단방향
//                startActivity(intent);

                //응답요구
                startActivityForResult(intent, REQUEST_RESULT_ACTIVITY);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_RESULT_ACTIVITY) {
            Log.e(TAG, "requestCode=" + requestCode);
            Log.e(TAG, "resultCode=" + resultCode);
            if(data !=null){
                Log.e(TAG, "data=" + data.getStringExtra(KEY_RESULT_TEST));
            }
        }

    }
}
