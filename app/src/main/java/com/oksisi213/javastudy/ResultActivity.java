package com.oksisi213.javastudy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    public static final String TAG = ResultActivity.class.getSimpleName();

    TextView email;
    TextView password;


    EditText result;
    Button previous;

    CheckBox check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        result = findViewById(R.id.result);
        previous = findViewById(R.id.previous);
        check = findViewById(R.id.check);

        Intent intent = getIntent();
        String id = intent.getStringExtra(InputActivity.KEY_ID);
        String pw = intent.getStringExtra(InputActivity.KEY_PW);
        Log.e(TAG, id + ":" + pw);

        email.setText(id);
        password.setText(pw);

//        Intent resultIntent = new Intent();
//        resultIntent.putExtra(InputActivity.KEY_RESULT_TEST, "Hello");
//        setResult(RESULT_OK, resultIntent);


        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int resultCode = check.isChecked() ? RESULT_OK : RESULT_CANCELED;
                String data = result.getText().toString();

                Intent resultIntent = new Intent();
                resultIntent.putExtra(InputActivity.KEY_RESULT_TEST, data);
                setResult(resultCode, resultIntent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }
}
