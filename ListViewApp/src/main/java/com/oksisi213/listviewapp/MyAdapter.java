package com.oksisi213.listviewapp;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Charles on 2018. 2. 18..
 */

public class MyAdapter extends BaseAdapter {
    public static final String TAG = MyAdapter.class.getSimpleName();

    private ArrayList<String> items = new ArrayList<>(); //data

    public void setItems(ArrayList<String> data) {
        items = data;
    }

    public void addItem(String title, String content) {
        Log.e(TAG, "addItem");
        items.add(title);
        notifyDataSetChanged();//데이터변경을 어댑터에게 알림. 어댑터는 getCount()에서 데이터의 사이즈를 보고 다시 getView()에서 뷰를 만듦
    }

    public void clear() {
        Log.e(TAG, "clear");
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        Log.e(TAG, "getCount");
        return items.size();//items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.e(TAG, "getView");
        View itemView = null;
        if (convertView != null) {
            itemView = convertView;//재활용
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);//뷰를 생성
            Log.e(TAG, "create child view");
        }

        TextView titleView = itemView.findViewById(R.id.title);
        TextView subTitleView = itemView.findViewById(R.id.subtitle);

        titleView.setText(items.get(position));
        subTitleView.setText(items.get(position));
        return itemView;
    }
}
