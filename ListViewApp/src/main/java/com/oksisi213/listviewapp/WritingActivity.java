package com.oksisi213.listviewapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class WritingActivity extends AppCompatActivity {
    public static final String TAG = WritingActivity.class.getSimpleName();
    EditText title, content;
    Button save, cancel;


    public static final String EXTRA_TITLE = "title"; //리터럴 상수, 유지보수 및 리팩토링에 용이, 사용자 코드 작성 오류 방지
    public static final String EXTRA_CONTENT = "content";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_writing);
        title = findViewById(R.id.title);
        content = findViewById(R.id.content);
        save = findViewById(R.id.save);
        cancel = findViewById(R.id.cancel);

        save.setOnClickListener(onClickListener);
        cancel.setOnClickListener(onClickListener);


    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.save:
                    save();
                    break;
                case R.id.cancel:
                    cancel();
                    break;
            }
        }
    };


    private void save() {
        Log.e(TAG, "save");
        String strTitle = title.getText().toString();
        String strContent = content.getText().toString();

        Intent resultData = new Intent();
        resultData.putExtra(EXTRA_TITLE, strTitle);
        resultData.putExtra(EXTRA_CONTENT, strContent);

        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

    private void cancel() {
        setResult(Activity.RESULT_CANCELED);// 꼭 쓸필요없음 , 기본값. 명시적으로 써주는 이유는? 개발자 코드 오류 방지
        finish();
    }

}
