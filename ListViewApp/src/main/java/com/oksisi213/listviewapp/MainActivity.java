package com.oksisi213.listviewapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQ_WRITING_ACTIVITY = 100;

    private static final int ID_MENU_GROUP_DEFAULT = 0;

    private static final int ID_MENU_WRITING = 1;
    private static final int ID_MENU_DELETE_ALL = 0;


    //1. Data Set 만들기
//    ArrayList<String> data = new ArrayList<>();

    //2. ListView 만들기
    ListView listView;

    //3. Adapter 만들기
    MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.listView);
        adapter = new MyAdapter();
        listView.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "onActivityResult");
        if (requestCode == REQ_WRITING_ACTIVITY && resultCode == Activity.RESULT_OK) {
            String strTitle = data.getStringExtra(WritingActivity.EXTRA_TITLE);
            String strContent = data.getStringExtra(WritingActivity.EXTRA_CONTENT);
            adapter.addItem(strTitle, strContent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(ID_MENU_GROUP_DEFAULT, ID_MENU_WRITING, ID_MENU_WRITING, "Write");
        menu.add(ID_MENU_GROUP_DEFAULT, ID_MENU_DELETE_ALL, ID_MENU_DELETE_ALL, "Delete all");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case ID_MENU_WRITING:
                startWritingActivity();
                break;
            case ID_MENU_DELETE_ALL:
                deleteAll();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteAll() {
        adapter.clear();
    }

    private void startWritingActivity() {
        Intent intent = new Intent();
        intent.setClass(MainActivity.this, WritingActivity.class);
        startActivityForResult(intent, REQ_WRITING_ACTIVITY);
    }
}
