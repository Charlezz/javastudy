package com.oksisi213.listviewapp2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView listView;

    MyAdapter adapter = new MyAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.listView);
        listView.setAdapter(adapter);


        ArrayList<MyData> items = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            MyData data = new MyData();
            data.title = "title " + i;
            data.description = "description " + i;
            data.position = i;

            items.add(data);
        }

//        "title0|description0"
//        "title1|description1"
//        "title2|description2"

        adapter.setItems(items);


    }

    private class MyAdapter extends BaseAdapter {

        private ArrayList<MyData> items = new ArrayList<>();

        public void setItems(ArrayList<MyData> items) {
            this.items = items;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, null);
            }
            TextView title = view.findViewById(R.id.text);
            TextView description = view.findViewById(R.id.text2);

            //data
            MyData data = items.get(position);
            String t1 = data.title;
            String t2 = data.description;
            int t3 = data.position;

            title.setText(t1);
            description.setText("");

            return view;
        }
    }
}
